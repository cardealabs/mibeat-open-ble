package com.cardea.beatbleopen;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

import static com.cardea.beatbleopen.Globals.*;
import static com.cardea.beatbleopen.file.FileUtils.*;
import static com.cardea.beatbleopen.plotutils.Chart.*;


public class PlottingActivity extends Activity {

    private static final String TAG = "PlottingActivity";
    Button save, restart;
    ProgressBar progressBar;

    //for plotting
    public static GraphicalView chart;
    LinearLayout chartContainer;
    private static XYMultipleSeriesDataset dataSet;
    private static XYMultipleSeriesRenderer multiRenderer;

    private Activity activity;
    private BluetoothGattCharacteristic bluetoothGattCharacteristic = HomeActivity.bluetoothGattCharacteristic;

    private boolean receiving = false;
    private CountDownTimer dataTimer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        waitingview();
    }

    @Override
    protected void onStop() {
        Log.e(TAG, "...onStop...");
        if (receiving)
            sendSignals(STOP);
        dataTimer.cancel();
        super.onStop();
    }

    // send specified byte 5 times on 100ms intervals
    private void sendSignals(final String what) {
        Log.e("SendSignal: ", what);
        new CountDownTimer(600, 100) {

            @Override
            public void onTick(long millisUntilFinished) {
                boolean isPresent = bluetoothGattCharacteristic != null;
                if (bluetoothGattCharacteristic != null && HomeActivity.mBluetoothLeService != null) {
                    bluetoothGattCharacteristic.setValue(what.getBytes());
                    HomeActivity.mBluetoothLeService.writeCharacteristic(bluetoothGattCharacteristic);
                    HomeActivity.mBluetoothLeService.setCharacteristicNotification(bluetoothGattCharacteristic, true);
                }
            }

            @Override
            public void onFinish() {
            }
        }.start();
    }

    /*Wait for 10 sec*/
    private void waitingview() {
        writeIndex = 0;
        sendSignals(START);
        sendSignals(FILTER_ON);
        receiving = true;
        setContentView(R.layout.recording);
        final TextView time_remain = (TextView) findViewById(R.id.textTime);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, 500); // see this max value coming back here, we animate towards that value
        animation.setDuration(10000); //in milliseconds
        animation.setInterpolator(new LinearInterpolator());
        animation.start();

        dataTimer = new CountDownTimer(10000, 1000) {
            public void onTick(long millisUntilFinished) {
                time_remain.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                progressBar.clearAnimation();
                sendSignals(STOP);
                receiving = false;
                setContentView(R.layout.activity_plotting);
                save = (Button) findViewById(R.id.btnSave);
                save.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        writeFile(activity, getBaseContext(), "BEAT " + selectedSignalType);
                        finish();
                    }
                });
                restart = (Button) findViewById(R.id.btnRestart);
                restart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        waitingview();
                    }
                });
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                chartContainer = (LinearLayout) findViewById(R.id.chartContainer);
                dataSet = new XYMultipleSeriesDataset();
                multiRenderer = new XYMultipleSeriesRenderer();
                drawGraph(samplingRateForDataAcquire, dataBuffer, writeIndex, dataSet, multiRenderer);
                chart = ChartFactory.getLineChartView(getBaseContext(), dataSet, multiRenderer);
                chartContainer.addView(chart);
                chart.repaint();
            }
        }.start();
    }

}
