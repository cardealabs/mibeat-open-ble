package com.cardea.beatbleopen;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cardea.beatbleopen.file.FileChooser;

import java.util.ArrayList;
import java.util.List;

import static com.cardea.beatbleopen.BluetoothLeService.MIBEAT_UUID;
import static com.cardea.beatbleopen.Globals.*;


public class HomeActivity extends Activity implements View.OnClickListener {
    // Debugging
    private static final String TAG = "HomeActivity";
    private static final boolean D = true;

    // Bluetooth Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_FILE_CHOOSER = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    // UI elements
    private Button getDataButton, ecg, emg, ppg, eog;
    private TextView statusTextView;

    // Name of the connected device
    private String mConnectedDeviceName = null;

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    private String mDeviceName;
    private String mDeviceAddress;
    private boolean connected = false;

    // Member object for the bluetooth service
//    public static BluetoothSetupService btService = null;
    public static BluetoothLeService mBluetoothLeService = null;

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            Log.e(TAG, "mBluetoothLeService service connected");
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                connected = true;
                Log.e("on receive: ","GATT CONNECTED");
                setStatus(R.string.title_connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                connected = false;
                Log.e("on receive: ","GATT DISCONNECTED");
                setStatus(R.string.title_not_connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
//                Log.e("on receive: ","GATT SERVICES DISCOVERED");
                populateBleGattCharacteristic(mBluetoothLeService.getSupportedGattServices());
            }
        }
    };
    public static BluetoothGattCharacteristic bluetoothGattCharacteristic;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (D) Log.d(TAG, "+++ ON CREATE +++");

        setContentView(R.layout.activity_bluetooth_setup);
        statusTextView = (TextView)findViewById(R.id.textViewStatus);

        getDataButton = (Button) findViewById(R.id.button_send);
        getDataButton.setOnClickListener(this);

        ecg = (Button) findViewById(R.id.btn_ecg);
        ecg.setOnClickListener(this);

        emg = (Button) findViewById(R.id.btn_emg);
        emg.setOnClickListener(this);

        ppg = (Button) findViewById(R.id.btn_ppg);
        ppg.setOnClickListener(this);

        eog = (Button) findViewById(R.id.btn_eog);
        eog.setOnClickListener(this);

        // Initialize the BluetoothSetupService to perform bluetooth connections
//        btService = new BluetoothSetupService(mHandler);
        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_LONG).show();
            finish();
        }

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if (D) Log.e(TAG, "+ ON RESUME +");

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
//        if (mBluetoothLeService != null) {
//            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
//            Log.d(TAG, "Connect request result=" + result);
//        }
    }


    @Override
    protected void onPause() {
        Log.e(TAG, "...onPause...");
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onStop() {
        Log.e(TAG, "...onStop...");

        super.onStop();
    }


    @Override
    protected void onDestroy() {
        Log.e(TAG, "...onDestroy...");
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    private void setStatus(int resId) {
        statusTextView.setText(resId);
    }

    private void setStatus(CharSequence subTitle) {
        statusTextView.setText(subTitle);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    setStatus("Connecting...");
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode != Activity.RESULT_OK) {
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            case REQUEST_FILE_CHOOSER:
                if (resultCode == Activity.RESULT_OK) {
                    String fileSelected = data.getStringExtra("fileSelected");
                    Toast.makeText(this, fileSelected, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(this, HistoryPlottingActivity.class);
                    i.putExtra("SELECTED_FILE_PATH", fileSelected);
                    startActivity(i);
                }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        mDeviceAddress = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Attempt to connect to the device
//        mBluetoothLeService.connect(mDeviceAddress);
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        MenuItem connectButton = menu.findItem(R.id.bt_connect_scan);
        if (connected)
            connectButton.setTitle("DISCONNECT");
        else
            connectButton.setTitle("CONNECT");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent serverIntent;
        switch (item.getItemId()) {
            case R.id.beat_history:
                Intent intent = new Intent(this, FileChooser.class);
                ArrayList<String> extensions = new ArrayList<>();
                extensions.add(".txt");
                intent.putStringArrayListExtra("filterFileExtension", extensions);
                startActivityForResult(intent, REQUEST_FILE_CHOOSER);
                return true;
            case R.id.bt_connect_scan:
                if (!connected) {
                    // Launch the DeviceListActivity to see devices and do scan
                    serverIntent = new Intent(this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                }
                else {
                    mBluetoothLeService.disconnect();
                }
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v==getDataButton) {
            Log.e("Connection State: ", ""+mBluetoothLeService.getConnectionState());
            if (mBluetoothLeService.getConnectionState() != BluetoothLeService.STATE_CONNECTED) {
                Toast.makeText(getBaseContext(), R.string.not_connected, Toast.LENGTH_SHORT).show();
            } else if (selectedSignalType.isEmpty()) {
                Toast.makeText(getBaseContext(), R.string.signal_not_selected, Toast.LENGTH_SHORT).show();
            } else {
                Intent i = new Intent(getBaseContext(), PlottingActivity.class);
                startActivityForResult(i, 0);
            }
        }
        if (v==ecg) {
            selectedSignalType = REQUEST_ECG;
            samplingRateForDataAcquire = ECG_SAMPLING_RATE;
            getDataButton.setText("Get ECG data");
        }
        if (v==emg) {
            selectedSignalType = REQUEST_EMG;
            samplingRateForDataAcquire = EMG_SAMPLING_RATE;
            getDataButton.setText("Get EMG data");
        }
        if (v==ppg) {
            selectedSignalType = REQUEST_PPG;
            samplingRateForDataAcquire = PPG_SAMPLING_RATE;
            getDataButton.setText("Get PPG data");
        }
        if (v==eog) {
            selectedSignalType = REQUEST_EOG;
            samplingRateForDataAcquire = EOG_SAMPLING_RATE;
            getDataButton.setText("Get EOG data");
        }
    }

    // Populate bluetoothGattCharacteristic
    private void populateBleGattCharacteristic(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                String uuid = gattCharacteristic.getUuid().toString();
                //Check if it is "HM_10"
                if(uuid.equals(MIBEAT_UUID.toString())){
                    Log.e("uuid: ", "found");
                    bluetoothGattCharacteristic = gattService.getCharacteristic(MIBEAT_UUID);
                    boolean isPresent = gattService.getCharacteristic(MIBEAT_UUID) != null;
                    Log.e("gatt charac present: ", ""+isPresent);

                }
            }
        }
    }
}
